export interface Movies {
    id: number;
    name: string;
    description: string;
    imgPath: string;
    duration: number;
    genre: string[];
    language: string;
    mpaaRating: MpaaRating;
    userRating: string;
}
export interface MpaaRating {
    type: string;
    label: string;
}
