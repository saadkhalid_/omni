import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'search'
})
export class SearchPipe implements PipeTransform {
  public transform(value, keys: string, term: string) {

    if (!term) return value;
    return (value || []).filter(item => keys.split(',').some(key => item.hasOwnProperty(key) && new RegExp(term, 'gi').test(item[key])));

  }
}


import { Component, OnInit } from '@angular/core';
import * as data from '../../assets/movies.json';
import { Movies } from '../movies.js';



@Component({
  selector: 'app-movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.css']
})
export class MoviesComponent implements OnInit {
  
  movies: Movies[] = (data as any).default;
  selectedMovie: Movies;
  movieDeatil: Boolean;

  constructor() { 
    this.movieDeatil = false;
  }
  ngOnInit() {
  }
  movieDetails(data?:Movies){
    this.movieDeatil = !this.movieDeatil;
     this.selectedMovie = data;
  }


}
